this is a an assignment for a job interview

for a clinic who just has a form and return if the tests are below or above the threshold

i added another page to be able to see what is stored in the DB
and api to add test thresholds

server: Scala
front end: uses the Scala templates (of Play Framework)
DB: Postgresql
Service: ElasticSearch

* the server was written on windows (might work on linux - haven't had time to try)

in order for the server to work we need:
- play framework 2.6
- postgresql best to try 9.4.1212 and above (however it should be backward compatible the driver and the db)
- elasticSearch 6.2.3V (this is what i used - but i am sure older version will work also)

the installation instruction will be for development mode:

we will define PROJECT_PATH as to were the project is fetched from git

the instruction are for windows

- install play framework https://www.playframework.com/documentation/2.6.x/Installing
- install postresql https://www.postgresql.org/download/
	- init DB:
		1. open "SQL Shell (psql)"
		2. open the folder PROJECT_PATH/DBScripts
		3. run the commends that are in  by the order of their name) - copy paste to sql shell
	- config postgresql with server: 
		1. make sure the DB is running
		2. go to application.conf in the PROJECT_PATH/clinics/conf/application.conf
		3. change:
			a. for db.default.url - the port to the port you choose the DB will run on (https://stackoverflow.com/questions/5598517/find-the-host-name-and-port-using-psql-commands)
			b. db.default.username to the user name who owns the DB (or another who has access)
			c. db.default.password to the password you choose to that user in that DB

- install elasticSearch https://www.elastic.co/downloads/elasticsearch
	- lets call the elasticSearch folder you installed ES
	start elasticSearch:
		- go to ES and run ES\bin\elasticsearch.bat (if you are on linux https://www.elastic.co/guide/en/elasticsearch/reference/2.1/setup-service.html)
	config elasticSearch with server:
		- if in the installation process or manually you changed the port then 
			you will need to change it in PROJECT_PATH/clinics/conf/application.conf
			otherwise the service shold just work (on localhost:9200)
			
- start the server
	- go to PROJECT_PATH/clinics/
	- in cmd type "sbt run"
	
- go to PROJECT_PATH/scripts/
	- run the script initServerWithData.bat"
	
if all went well then you should have a web site on localhost:9000

try:
	http://localhost:9000/ for the blood_test threshold to see the
	http://localhost:9000/testsThresholds/list - to list the thresholds and possible tests
	look at the build.sbt to see the available controllers
	

* i think on linux you have to access the ports for the DB and ES
DB- port: 5432
ES- port: 9200


TODO:
- add a script to index the DB to ElasticSearch (currently happens on add - what if add to elasticSearch fails)
- write an add that adds to the DB in a batch the thresholds
- create a script to install server


