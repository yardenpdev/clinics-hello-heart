package services

import javax.inject._
import play.api._
import play.api.data._
import play.api.mvc._
import play.api.libs.ws._
import scala.concurrent.Future
import java.util.concurrent.atomic.AtomicInteger
import play.api.libs.json._
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import play.libs.ws.ahc._
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.Configuration

case class ElasticSearchService @Inject()(ws: WSClient, config: Configuration) {

    val elasticUrl = config.underlying.getString("es.elasticSearchHost")
    val docString = "_doc"
    val searchString = "_search"
    var atomicInteger : AtomicInteger = new AtomicInteger(0)

    // adds a doc to the node nodeName
    // returns true or false if succesed to add the doc
    // timeout of 1 second
    def addDocToNode(doc: String, nodeName: String, field:String):Future[Boolean] = {
      
	  // curl -X PUT "localhost:9200/test_names/_doc/1?pretty&pretty" -H "Content-Type: application/json" -d "{ \"name\":\"Jhon Doe\" }"
     	val url: String = elasticUrl + nodeName + "/" + docString + "/" + atomicInteger.addAndGet(1)
      	val request: WSRequest = ws.url(url)
      								.addHttpHeaders("Content-Type" -> "application/json")
      								.withRequestTimeout(1000.millis)
      	val data = Json.obj(field -> doc)
      	val futureResponse:Future[WSResponse] = request.put(data)
      	futureResponse.map{ response => {
      		 	println(response.json);
      		 	true 
      		}
	  	}.recover {
			case e: Exception => { println("couldn't add to ElasticSearchService:" + e + "\ncheck that elasticSearch is service is active"); false }
		}
    }

    // retriving a suggestion for the searched string in the nodeName in field givven
    // return None if no connection or no results found
    // returns the first suggestions for the doc
    def suggestTestName(searched:String, nodeName: String, field:String):Future[Option[String]] = {
		val url:String = elasticUrl + nodeName +"/" + searchString + "/"
		val data = Json.obj("query" -> Json.obj("match" -> Json.obj(field -> searched)),
							"suggest" -> Json.obj("my-suggestion" -> Json.obj("text" -> searched,
							 												  "term" -> Json.obj("field" -> "test_name", "suggest_mode" -> "missing"))))
			// """
			// 					  "query" : {
			// 					    "match": {
			// 					      "message": searched
			// 					    }
			// 					  },
			// 					  "suggest" : {
			// 					    "my-suggestion" : {
			// 					      "text" : searched,
			// 						  "suggest_mode": "missing"
			// 					      "term" : {
			// 					        "field" : "test_name"
			// 					      }
			// 					    }
			// 					  }
			// 					}""")
		val request: WSRequest = ws.url(url)
      								.addHttpHeaders("Content-Type" -> "application/json")
		val futureResponse:Future[WSResponse] = request.post(data)
	  	futureResponse.map { response =>
	  		val responseJson = response.json
	  		val hits = (responseJson \ "hits" \ "hits").as[JsArray]
		  	if(hits.value.size == 0) {
			  	val suggestionsArray = (response.json \ "suggest" \ "my-suggestion").as[JsArray]
			  	if(suggestionsArray.value.size > 0) {
			  		val optionsArray = (suggestionsArray.value(0) \ "options").as[JsArray]
			  		if(optionsArray.value.size > 0) {
			  			val suggestedJson = optionsArray.value(0)
				  		Some((suggestedJson \ "text").as[String])
			  		}
			  		else {
			  			None
			  		}
			  	}
			  	else {
			  		None
			  	}
			}
			else {
				Some((hits.value(0) \ "_source" \ field).as[String])
			}
		}
		.recover {
			case e: Exception => { println("couldn't get suggestions from ElasticSearchService:" + e + "\ncheck that elasticSearch is service is active"); None }
		}
    }

}
