package models

case class BloodTestThreshold(testName: String, category:String, threshold: Int)