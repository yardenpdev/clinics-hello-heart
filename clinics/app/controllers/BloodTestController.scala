package controllers

import javax.inject._
import play.api._
import play.api.data._
import play.api.mvc._
import models.BloodTestThreshold
import scala.concurrent.ExecutionContext
import scala.concurrent.Future

/**
 * This controller creates an `Action` blood test request and display
 */
@Singleton
class BloodTestController @Inject()(cc: MessagesControllerComponents, thresholdsDAO:BloodTestThresholdDAO)(implicit ec: ExecutionContext) extends MessagesAbstractController(cc) {
  import BloodTestForm._

  private val postUrl = routes.BloodTestController.testBloodResults()

  def index() = Action { implicit request: MessagesRequest[AnyContent] =>
    Ok(views.html.index(form, postUrl))
  }

    // This will be the action that handles our form post
  def testBloodResults = Action.async { implicit request: MessagesRequest[AnyContent] =>
    val errorFunction = { formWithErrors: Form[Data] =>
      Future(BadRequest(views.html.index(formWithErrors, postUrl)))
    }

    val successFunction = { data: Data =>
      val resultTuple = thresholdsDAO.isUnderThreshold(data.testName, data.testResult)
      resultTuple.map( res => {
        (res._1, res._2, res._3) match {
          case (isUnderThreshold: Boolean, searched: String, foundQuery:Boolean) => { 
            Redirect(routes.BloodTestController.index()).flashing("search for test" -> searched, "result" -> resultMessage(isUnderThreshold, foundQuery))
          }
        }
      })
    }

    val formValidationResult = form.bindFromRequest
    formValidationResult.fold(errorFunction, successFunction)
  }

  // helper function to display to the user
  def resultMessage(isUnderThreshold:Boolean, foundQuery:Boolean):String = {
    if(foundQuery) {
      if(isUnderThreshold) "Good!"
      else "Bad :/"
    }
    else {
      "no test found by that name"
    }
  }
}
