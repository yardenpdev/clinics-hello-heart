package controllers

import javax.inject._
import play.api._
import play.api.data._
import play.api.mvc._
import play.api.libs.json._
import models.BloodTestThreshold
import play.api.libs.ws._
import services.ElasticSearchService
import scala.concurrent.Future
import play.api.db._
import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
case class BloodTestThresholdDAO @Inject()(db: Database, es:ElasticSearchService) {

    val elastic_node_name = "test_names"
    val thresholdsTableName = "blood_tests_thresholds"

    // addes a BloodTestThresholds to the DB
    def add(value:BloodTestThreshold):Future[Boolean] = {
      var added = false
      try {
        db.withConnection  { conn => 

              val stmt = conn.createStatement
              // test_name | category | threshold
              val testNameV = value.testName
              val categoryV = value.category
              val thresholdV = value.threshold
              val queryString = f"""INSERT INTO $thresholdsTableName%s (test_name, category, threshold) 
                                                                VALUES ('$testNameV%s', '$categoryV%s', '$thresholdV%d');"""
              val numberOfUpdates:Int = stmt.executeUpdate(queryString)
              added = true
          }
      }
      catch {
        case e: Exception => println("something bad happend with the db insert:" + e)
      }
      if(added){
        es.addDocToNode(value.testName, elastic_node_name, "test_name")
      }
      Future(added)
    }

    // test if the testName is under the defined threshold defined in the DB
    def isUnderThreshold(testName:String, testResult:Int):Future[(Boolean, String, Boolean)] = {
      val suggestionF:Future[Option[String]] = es.suggestTestName(testName, elastic_node_name, "test_name")
      val isUnderThreshold = false
      try{
          val resultF = suggestionF.map{ sugOpt =>
                val sug:String = sugOpt.getOrElse(testName)
                db.withConnection{ conn =>
                  var isUnderThreshold:Boolean = false
                  var searched:String = "n/a"
                  var found:Boolean = false 
                  val stmt = conn.createStatement 
                  val queryString = f"SELECT * FROM $thresholdsTableName%s WHERE $thresholdsTableName%s.test_name LIKE '$sug%s';"
                  val rs = stmt.executeQuery(queryString)
                  if(!rs.isBeforeFirst()) {
                    // the result set is empty
                    isUnderThreshold = false
                    searched = sug
                    found = false
                  }
                  else {
                    while(rs.next()) {
                      val testName = rs.getString("test_name")
                      val category = rs.getString("category")
                      val threshold = rs.getInt("threshold")
                      if(threshold > testResult) {
                        isUnderThreshold = true
                        searched = sug
                        found = true
                      }
                      else {
                        isUnderThreshold = false
                        searched = sug
                        found = true
                      }
                    }
                  }
                  (isUnderThreshold, searched, found)
              }
        }
        return resultF
      }
      catch {
         case e: Exception => { 
           println("something bad happend with isUnderThreshold:" + e)
           return Future((false, "server Error", false))
         }

      } 
    }

    // retrive all the thresholds pressisted in the DB
    def getAllThrasholds():Seq[BloodTestThreshold] = {
      var resultList = ListBuffer[BloodTestThreshold]()
      try {
        db.withConnection  { conn => 
              val stmt = conn.createStatement
              // test_name | category | threshold
              val queryString = f"SELECT * from $thresholdsTableName%s;"
              val rs = stmt.executeQuery(queryString)
              while(rs.next()) {
                val testName = rs.getString("test_name")
                val category = rs.getString("category")
                val threshold = rs.getString("threshold")
                resultList += new BloodTestThreshold(testName, category, threshold.toInt)
              }
          }
      }
      catch {
         case e: Exception => println("something bad happend with getAllThrasholds:" + e)
      }
      resultList.toList
    }
}

// controls the access and adding of thresholds to the DB and from the DB
class BloodTestThresholdController @Inject()(cc: ControllerComponents, thresholdsDAO:BloodTestThresholdDAO) extends AbstractController(cc) {

  // add to the DB from a json object a bloodTestThreshold
  def add() = Action.async(parse.json) { request : Request[JsValue] =>
    val jsValue = request.body
    val addedList:Seq[Future[(String, Boolean)]] = (jsValue \ "thresholds").as[List[JsValue]].map(json => {
      val testName = (json \ "testName").as[String]
      val category = (json \ "category").as[String]
      val threshold = (json \ "threshold").as[Int]
      val value = new BloodTestThreshold(testName, category, threshold)
      // add to DB
      thresholdsDAO.add(value).map(added => (value.testName, added))
      })
    val addedListF:Future[Seq[(String, Boolean)]] = Future.sequence(addedList)
    var resultAdding = addedListF.map(res => res.aggregate("")((acc: String, tuple: (String, Boolean)) => {
        acc + "(" + tuple._1 + "," + tuple._2 + "), "
      }, _ + _))
    resultAdding.map(result => Ok("added test:" + result))
  }

  // lists all the BloodTestsThresholds exist in the DB  
  def listBloodTestsThresholds = Action { implicit request: Request[AnyContent] =>
    val bloodTestsThresholds = thresholdsDAO.getAllThrasholds()
    Ok(views.html.listThresholds(bloodTestsThresholds))
  }
}
