package controllers

object BloodTestForm {
  import play.api.data.Forms._
  import play.api.data.Form

  case class Data(testName: String, testResult: Int)

  val form = Form(
    mapping(
      "Test Name" -> nonEmptyText,
      "Blood Test Result" -> number
    )(Data.apply)(Data.unapply)
  )
}