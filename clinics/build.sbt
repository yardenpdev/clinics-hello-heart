name := """clinics"""
organization := "hello-heart"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
	ws,
	jdbc,
	"org.postgresql" % "postgresql" % "9.4.1212",
	ehcache,
	guice,
	"org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
)

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "hello-heart.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "hello-heart.binders._"
